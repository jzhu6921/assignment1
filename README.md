**Project owned by
Jinhui Zhu for school use

---

## Setup

1. Click **Source** on the left side.
2. Download repository
3. Unzip
4. Double click Default.html

---

## License

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

For more information, please refer to <https://unlicense.org>

I choose this license because this this project does not have commercial values.